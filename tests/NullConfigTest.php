<?php
// NullConfigTest.php
// Created: 2023-10-20
// Updated: 2023-10-20

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;

#[CoversClass(Syokuhou\NullConfig::class)]
#[CoversClass(Syokuhou\GetValuesTrait::class)]
final class NullConfigTest extends TestCase {
    public function testNullConfig(): void {
        $config = new \Syokuhou\NullConfig;

        // no-ops but run anyway to ensure no screaming
        $config->removeValues('test');
        $config->setString('stringval', 'the');
        $config->setInteger('intval', 1234);
        $config->setFloat('floatval', 56.78);
        $config->setBoolean('boolval', true);
        $config->setArray('arrval', ['meow']);
        $config->setValues([
            'stringval' => 'the',
            'intval' => 1234,
            'floatval' => 56.78,
            'boolval' => true,
            'arrval' => ['meow'],
        ]);

        // NullConfig currently returns itself when scoping
        // might change this depending on whether the scope prefix will be exposed or not
        $scoped = $config->scopeTo('scoped');
        $this->assertEquals($config, $scoped);

        // hasValues should always return true when the list is empty
        $this->assertTrue($config->hasValues([]));
        $this->assertFalse($config->hasValues('anything'));
        $this->assertFalse($config->hasValues(['manything1', 'manything2']));

        $this->assertEmpty($config->getAllValueInfos());
        $this->assertEmpty($config->getValueInfos('the'));

        $expected = [
            'test_no_type' => null,
            'test_yes_type' => false,
            'test_no_type_yes_default' => 1234,
            'test_yes_type_yes_default' => 56.78,
            'aliased' => null,
        ];
        $values = $config->getValues([
            'test_no_type',
            'test_yes_type:b',
            ['test_no_type_yes_default', 1234],
            ['test_yes_type_yes_default:d', 56.78],
            ['test_no_default_yes_alias', null, 'aliased'],
        ]);

        $this->assertEqualsCanonicalizing($expected, $values);

        $this->assertNull($config->getValueInfo('value'));

        $this->assertEquals('', $config->getString('string'));
        $this->assertEquals('default', $config->getString('string', 'default'));

        $this->assertEquals(0, $config->getInteger('int'));
        $this->assertEquals(960, $config->getInteger('int', 960));

        $this->assertEquals(0, $config->getFloat('float'));
        $this->assertEquals(67.7, $config->getFloat('float', 67.7));

        $this->assertFalse($config->getBoolean('bool'));
        $this->assertTrue($config->getBoolean('bool', true));

        $this->assertEmpty($config->getArray('arr'));
        $this->assertEqualsCanonicalizing(['de', 'het', 'een'], $config->getArray('the', ['de', 'het', 'een']));
    }
}
