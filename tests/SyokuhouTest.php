<?php
// SyokuhouTest.php
// Created: 2023-10-20
// Updated: 2023-10-20

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;

#[CoversClass(Syokuhou\Syokuhou::class)]
final class SyokuhouTest extends TestCase {
    public function testVersionDecode(): void {
        $expected = trim(file_get_contents(__DIR__ . '/../VERSION'));
        $this->assertEquals($expected, \Syokuhou\Syokuhou::version());
    }
}
