# Syokuhou

This library has been deprecated and its functionality is now implemented into [Index](https://patchii.net/flash/index).

## Original header

Syokuhou is a common library for configuration in my PHP projects.
It provides both a file interface and a database interface out of the box as well as scoping.


## Requirements and Dependencies

Syokuhou currently targets **PHP 8.3**.

### `Syokuhou\DbConfig`

A compatible `Index\Data\IDbConnection` must be provided.


## Versioning

Syokuhou versioning will follows the [Semantic Versioning specification v2.0.0](https://semver.org/spec/v2.0.0.html).

Previous major versions may be supported for a time with backports depending on what projects of mine still target older versions of PHP.

The version is stored in the root of the repository in a file called `VERSION` and can be read out within Syokuhou using `Syokuhou\SyokuhouInfo::getVersion()`.


## Contribution

By submitting code for inclusion in the main Syokuhou source tree you agree to transfer ownership of the code to the project owner.
The contributor will still be attributed for the contributed code, unless they ask for this attribution to be removed.
This is to avoid intellectual property rights traps and drama that could lead to blackmail situations.
If you do not agree with these terms, you are free to fork off.


## Licencing

Syokuhou is available under the BSD 3-Clause Clear License, a full version of which is enclosed in the LICENCE file.
