<?php
// IConfig.php
// Created: 2023-10-20
// Updated: 2023-10-20

namespace Syokuhou;

/**
 * Provides a common interface for configuration providers.
 */
interface IConfig {
    /**
     * Creates a scoped configuration instance that prepends a prefix to all names.
     *
     * @param non-empty-array<string> ...$prefix Parts of the desired.
     * @return IConfig A scoped configuration instance.
     */
    public function scopeTo(string ...$prefix): IConfig;

    /**
     * Gets the separator character used for scoping.
     *
     * @return string Separator character.
     */
    public function getSeparator(): string;

    /**
     * Checks if configurations contains given names.
     * An empty $names list will always return true.
     *
     * @param string|string[] $names Name or names to check for.
     * @return bool Whether all given names are present or not.
     */
    public function hasValues(string|array $names): bool;

    /**
     * Removes values with given names from the configuration, if writable.
     *
     * @param string|string[] $names Name or names to remove.
     * @throws \RuntimeException If the configuration is read only.
     */
    public function removeValues(string|array $names): void;

    /**
     * Lists all value informations from this configuration.
     *
     * @param int $range Amount of items to take, 0 for all. Must be a positive integer.
     * @param int $offset Amount of items to skip. Must be a positive integer. Has no effect if $range is 0.
     * @throws \InvalidArgumentException If $range or $offset are negative.
     * @return IConfigValueInfo[] Configuration value infos.
     */
    public function getAllValueInfos(int $range = 0, int $offset = 0): array;

    /**
     * Gets value informations for one or more items.
     *
     * @param string|string[] $names Name or names to retrieve.
     * @return IConfigValueInfo[] Array with value informations.
     */
    public function getValueInfos(string|array $names): array;

    /**
     * Gets value information for a single item.
     *
     * @param string $name Name or names to retrieve.
     * @return ?IConfigValueInfo Value information, or null if not present.
     */
    public function getValueInfo(string $name): ?IConfigValueInfo;

    /**
     * Gets multiple values of varying types at once.
     *
     * The format of a $specs entry can be one of the following:
     *  If the entry is a string:
     *   - The name of a value: 'value_name'
     *   - The name of a value followed by a requested type, separated by a colon: 'value_name:s', 'value_name:i', 'value_name:a'
     *  If the entry is an array, all except [0] are optional:
     *   - [0] follows to same format as the above described string
     *   - [1] is the default value to fall back on, MUST be the same type as the one specified in [0].
     *   - [2] is an alternative key for the output array.
     *
     * Available types are:
     *  :s - string
     *  :a - array
     *  :i - integer
     *  :b - boolean
     *  :f - float
     *  :d - float
     *
     * @param array<string|string[]> $specs Specification of what items to grab.
     * @throws \InvalidArgumentException If $specs is malformed.
     * @return array<string, mixed> An associative array containing the retrieved values.
     */
    public function getValues(array $specs): array;

    /**
     * Gets a single string value.
     *
     * @param string $name Name of the value to fetch.
     * @param string $default Default value to fall back on if the value is not present.
     * @return string Configuration value for $name.
     */
    public function getString(string $name, string $default = ''): string;

    /**
     * Gets a single integer value.
     *
     * @param string $name Name of the value to fetch.
     * @param int $default Default value to fall back on if the value is not present.
     * @return int Configuration value for $name.
     */
    public function getInteger(string $name, int $default = 0): int;

    /**
     * Gets a single floating point value.
     *
     * @param string $name Name of the value to fetch.
     * @param float $default Default value to fall back on if the value is not present.
     * @return float Configuration value for $name.
     */
    public function getFloat(string $name, float $default = 0): float;

    /**
     * Gets a single boolean value.
     *
     * @param string $name Name of the value to fetch.
     * @param bool $default Default value to fall back on if the value is not present.
     * @return bool Configuration value for $name.
     */
    public function getBoolean(string $name, bool $default = false): bool;

    /**
     * Gets an array value.
     *
     * @param string $name Name of the value to fetch.
     * @param mixed[] $default Default value to fall back on if the value is not present.
     * @return mixed[] Configuration value for $name.
     */
    public function getArray(string $name, array $default = []): array;

    /**
     * Sets multiple values at once using an associative array.
     *
     * @param array<string, mixed> $values Values to save.
     * @throws \InvalidArgumentException If $values is malformed.
     * @throws \RuntimeException If the configuration is read only.
     */
    public function setValues(array $values): void;

    /**
     * Sets a single string value.
     *
     * @param string $name Name of the value to save.
     * @param string $value Value to save.
     */
    public function setString(string $name, string $value): void;

    /**
     * Sets a single integer value.
     *
     * @param string $name Name of the value to save.
     * @param int $value Value to save.
     */
    public function setInteger(string $name, int $value): void;

    /**
     * Sets a single floating point value.
     *
     * @param string $name Name of the value to save.
     * @param float $value Value to save.
     */
    public function setFloat(string $name, float $value): void;

    /**
     * Sets a single boolean value.
     *
     * @param string $name Name of the value to save.
     * @param bool $value Value to save.
     */
    public function setBoolean(string $name, bool $value): void;

    /**
     * Sets an array value.
     *
     * @param string $name Name of the value to save.
     * @param mixed[] $value Value to save.
     */
    public function setArray(string $name, array $value): void;
}
