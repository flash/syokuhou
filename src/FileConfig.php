<?php
// FileConfig.php
// Created: 2023-10-20
// Updated: 2024-06-03

namespace Syokuhou;

use InvalidArgumentException;
use Index\IO\FileStream;
use Index\IO\Stream;

/**
 * Provides a configuration in Sharp Chat format.
 */
class FileConfig implements IConfig {
    use ImmutableConfigTrait, GetValueInfoTrait, GetValuesTrait;

    /**
     * @param array<string, FileConfigValueInfo> $values
     */
    public function __construct(private array $values) {}

    public function getSeparator(): string {
        return ':';
    }

    public function scopeTo(string ...$prefix): IConfig {
        return new ScopedConfig($this, $prefix);
    }

    public function hasValues(string|array $names): bool {
        if(is_string($names))
            return array_key_exists($names, $this->values);

        foreach($names as $name)
            if(!array_key_exists($name, $this->values))
                return false;

        return true;
    }

    public function getAllValueInfos(int $range = 0, int $offset = 0): array {
        if($range === 0)
            return array_values($this->values);

        if($range < 0)
            throw new InvalidArgumentException('$range must be a positive integer.');
        if($offset < 0)
            throw new InvalidArgumentException('$offset must be greater than zero if a range is specified.');

        return array_slice($this->values, $offset, $range);
    }

    public function getValueInfos(string|array $names): array {
        if(is_string($names))
            return array_key_exists($names, $this->values) ? [$this->values[$names]] : [];

        $infos = [];

        foreach($names as $name)
            if(array_key_exists($name, $this->values))
                $infos[] = $this->values[$name];

        return $infos;
    }

    /**
     * Creates an instance of FileConfig from an array of lines.
     *
     * @param string[] $lines Config lines.
     * @return FileConfig
     */
    public static function fromLines(array $lines): self {
        $values = [];

        foreach($lines as $line) {
            $line = trim($line);
            if($line === '' || $line[0] === '#' || $line[0] === ';')
                continue;

            $info = new FileConfigValueInfo(...explode(' ', $line, 2));
            $values[$info->getName()] = $info;
        }

        return new FileConfig($values);
    }

    /**
     * Creates an instance of FileConfig from a string.
     *
     * @param string $lines Config lines.
     * @param non-empty-string $newLine Line separator character.
     * @return FileConfig
     */
    public static function fromString(string $lines, string $newLine = "\n"): self {
        return self::fromLines(explode($newLine, $lines));
    }

    /**
     * Creates an instance of FileConfig from a file.
     *
     * @param string $path Config file path.
     * @throws InvalidArgumentException If $path does not exist.
     * @return FileConfig
     */
    public static function fromFile(string $path): self {
        if(!is_file($path))
            throw new InvalidArgumentException('$path does not exist.');

        return self::fromStream(FileStream::openRead($path));
    }

    /**
     * Creates an instance of FileConfig from a readable stream.
     *
     * @param Stream $stream Config file stream.
     * @throws InvalidArgumentException If $stream is not readable.
     * @return FileConfig
     */
    public static function fromStream(Stream $stream): self {
        if(!$stream->canRead())
            throw new InvalidArgumentException('$stream must be readable.');

        $values = [];
        while(($line = $stream->readLine()) !== null) {
            $line = trim($line);
            if($line === '' || $line[0] === '#' || $line[0] === ';')
                continue;

            $info = new FileConfigValueInfo(...explode(' ', $line, 2));
            $values[$info->getName()] = $info;
        }

        return new FileConfig($values);
    }
}
