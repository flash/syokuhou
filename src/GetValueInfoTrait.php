<?php
// GetValueInfoTrait.php
// Created: 2023-10-20
// Updated: 2023-10-20

namespace Syokuhou;

/**
 * Provides implementations for things that are essentially macros for {@see IConfig::getValueInfos}.
 */
trait GetValueInfoTrait {
    public function getValueInfo(string $name): ?IConfigValueInfo {
        $infos = $this->getValueInfos($name);
        return empty($infos) ? null : $infos[0];
    }

    public function getString(string $name, string $default = ''): string {
        $valueInfo = $this->getValueInfo($name);
        return $valueInfo?->isString() ? $valueInfo->getString() : $default;
    }

    public function getInteger(string $name, int $default = 0): int {
        $valueInfo = $this->getValueInfo($name);
        return $valueInfo?->isInteger() ? $valueInfo->getInteger() : $default;
    }

    public function getFloat(string $name, float $default = 0): float {
        $valueInfo = $this->getValueInfo($name);
        return $valueInfo?->isFloat() ? $valueInfo->getFloat() : $default;
    }

    public function getBoolean(string $name, bool $default = false): bool {
        $valueInfo = $this->getValueInfo($name);
        return $valueInfo?->isBoolean() ? $valueInfo->getBoolean() : $default;
    }

    public function getArray(string $name, array $default = []): array {
        $valueInfo = $this->getValueInfo($name);
        return $valueInfo?->isArray() ? $valueInfo->getArray() : $default;
    }
}
