<?php
// MutableConfigTrait.php
// Created: 2023-10-20
// Updated: 2023-10-20

namespace Syokuhou;

/**
 * Defines set aliases so you don't have to.
 */
trait MutableConfigTrait {
    public function setString(string $name, string $value): void {
        $this->setValues([$name => $value]);
    }

    public function setInteger(string $name, int $value): void {
        $this->setValues([$name => $value]);
    }

    public function setFloat(string $name, float $value): void {
        $this->setValues([$name => $value]);
    }

    public function setBoolean(string $name, bool $value): void {
        $this->setValues([$name => $value]);
    }

    public function setArray(string $name, array $value): void {
        $this->setValues([$name => $value]);
    }
}
