<?php
// DbConfigValueInfo.php
// Created: 2023-10-20
// Updated: 2023-10-20

namespace Syokuhou;

use UnexpectedValueException;
use Index\Data\IDbResult;

/**
 * Provides information about a databased configuration value.
 */
class DbConfigValueInfo implements IConfigValueInfo {
    private string $name;
    private string $value;

    /** @internal */
    public function __construct(IDbResult $result) {
        $this->name = $result->getString(0);
        $this->value = $result->getString(1);
    }

    public function getName(): string {
        return $this->name;
    }

    public function getType(): string {
        return match($this->value[0]) {
            's' => 'string',
            'a' => 'array',
            'i' => 'int',
            'b' => 'bool',
            'd' => 'float',
            default => 'unknown',
        };
    }

    public function isString():  bool { return $this->value[0] === 's'; }
    public function isInteger(): bool { return $this->value[0] === 'i'; }
    public function isFloat():   bool { return $this->value[0] === 'd'; }
    public function isBoolean(): bool { return $this->value[0] === 'b'; }
    public function isArray():   bool { return $this->value[0] === 'a'; }

    public function getValue(): mixed {
        return unserialize($this->value);
    }

    public function getString(): string {
        $value = $this->getValue();
        if(!is_string($value))
            throw new UnexpectedValueException('Value is not a string.');
        return $value;
    }

    public function getInteger(): int {
        $value = $this->getValue();
        if(!is_int($value))
            throw new UnexpectedValueException('Value is not an integer.');
        return $value;
    }

    public function getFloat(): float {
        $value = $this->getValue();
        if(!is_float($value))
            throw new UnexpectedValueException('Value is not a floating point number.');
        return $value;
    }

    public function getBoolean(): bool {
        $value = $this->getValue();
        if(!is_bool($value))
            throw new UnexpectedValueException('Value is not a boolean.');
        return $value;
    }

    public function getArray(): array {
        $value = $this->getValue();
        if(!is_array($value))
            throw new UnexpectedValueException('Value is not an array.');
        return $value;
    }

    public function __toString(): string {
        $value = $this->getValue();
        if(is_array($value))
            return implode(', ', $value);

        return (string)$value; // @phpstan-ignore-line dude trust me
    }
}
