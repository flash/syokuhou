<?php
// NullConfig.php
// Created: 2023-10-20
// Updated: 2023-10-20

namespace Syokuhou;

/**
 * Provides a black hole configuration that will always return the default values.
 */
class NullConfig implements IConfig {
    use GetValuesTrait;

    public function __construct() {}

    public function getSeparator(): string {
        return "\0";
    }

    public function scopeTo(string ...$prefix): IConfig {
        return $this;
    }

    public function hasValues(string|array $names): bool {
        return is_array($names) && empty($names);
    }

    public function getAllValueInfos(int $range = 0, int $offset = 0): array {
        return [];
    }

    public function getValueInfos(string|array $names): array {
        return [];
    }

    public function getValueInfo(string $name): ?IConfigValueInfo {
        return null;
    }

    public function getString(string $name, string $default = ''): string {
        return $default;
    }

    public function getInteger(string $name, int $default = 0): int {
        return $default;
    }

    public function getFloat(string $name, float $default = 0): float {
        return $default;
    }

    public function getBoolean(string $name, bool $default = false): bool {
        return $default;
    }

    public function getArray(string $name, array $default = []): array {
        return $default;
    }

    public function removeValues(string|array $names): void {}
    public function setValues(array $values): void {}
    public function setString(string $name, string $value): void {}
    public function setInteger(string $name, int $value): void {}
    public function setFloat(string $name, float $value): void {}
    public function setBoolean(string $name, bool $value): void {}
    public function setArray(string $name, array $value): void {}
}
