<?php
// SharpConfig.php
// Created: 2024-06-03
// Updated: 2024-06-03

namespace Syokuhou;

/**
 * Provides an alias for FileConfig.
 */
class SharpConfig extends FileConfig {}
