<?php
// SyokuhouInfo.php
// Created: 2023-10-20
// Updated: 2023-10-20

namespace Syokuhou;

/**
 * Provides information about the Syokuhou library.
 */
final class Syokuhou {
    public const PATH_SOURCE = __DIR__;
    public const PATH_ROOT = self::PATH_SOURCE . DIRECTORY_SEPARATOR . '..';
    public const PATH_VERSION = self::PATH_ROOT . DIRECTORY_SEPARATOR . 'VERSION';

    /**
     * Gets the current version of the Syokuhou library.
     *
     * Reads the VERSION file in the root of the Syokuhou directory.
     * Returns 0.0.0 if reading the file failed for any reason.
     *
     * @return string Current version string.
     */
    public static function version(): string {
        if(!is_file(self::PATH_VERSION))
            return '0.0.0';

        $version = file_get_contents(self::PATH_VERSION);
        if($version === false)
            return '0.0.0';

        return trim($version);
    }
}
