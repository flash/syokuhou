<?php
// IConfigValueInfo.php
// Created: 2023-10-20
// Updated: 2023-10-20

namespace Syokuhou;

use Stringable;

/**
 * Provides a common interface for configuration values.
 */
interface IConfigValueInfo extends Stringable {
    /**
     * Gets the name of this configuration value.
     *
     * @return string Configuration value name.
     */
    public function getName(): string;

    /**
     * Gets the type of this configuration value.
     *
     * @return string Configuration value type name.
     */
    public function getType(): string;

    /**
     * Checks whether the value is a string.
     *
     * @return bool True if the value is a string.
     */
    public function isString(): bool;

    /**
     * Checks whether the value is an integer.
     *
     * @return bool True if the value is an integer.
     */
    public function isInteger(): bool;

    /**
     * Checks whether the value is a floating point number.
     *
     * @return bool True if the value is a floating point number.
     */
    public function isFloat(): bool;

    /**
     * Checks whether the value is a boolean.
     *
     * @return bool True if the value is a boolean.
     */
    public function isBoolean(): bool;

    /**
     * Checks whether the value is an array.
     *
     * @return bool True if the value is an array.
     */
    public function isArray(): bool;

    /**
     * Gets the raw value without any type validation.
     *
     * @return mixed The configuration value.
     */
    public function getValue(): mixed;

    /**
     * Ensures the value is a string and returns the value.
     *
     * @throws \UnexpectedValueException If the value is not a string.
     * @return string String configuration value.
     */
    public function getString(): string;

    /**
     * Ensures the value is an integer and returns the value.
     *
     * @throws \UnexpectedValueException If the value is not an integer.
     * @return int Integer configuration value.
     */
    public function getInteger(): int;

    /**
     * Ensures the value is a floating point number and returns the value.
     *
     * @throws \UnexpectedValueException If the value is not a floating point number.
     * @return float Floating point number configuration value.
     */
    public function getFloat(): float;

    /**
     * Ensures the value is a boolean and returns the value.
     *
     * @throws \UnexpectedValueException If the value is not a boolean.
     * @return bool Boolean configuration value.
     */
    public function getBoolean(): bool;

    /**
     * Ensures the value is an array and returns the value.
     *
     * @throws \UnexpectedValueException If the value is not an array.
     * @return mixed[] Array configuration value.
     */
    public function getArray(): array;
}
