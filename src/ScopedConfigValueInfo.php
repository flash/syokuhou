<?php
// ScopedConfigValueInfo.php
// Created: 2023-10-20
// Updated: 2023-10-20

namespace Syokuhou;

/**
 * Provides information about a scoped configuration value.
 */
class ScopedConfigValueInfo implements IConfigValueInfo {
    /** @internal */
    public function __construct(
        private IConfigValueInfo $info,
        private int $prefixLength
    ) {}

    public function getName(): string {
        return substr($this->info->getName(), $this->prefixLength);
    }

    /**
     * Gets the real name of the configuration value, without removing the prefix.
     *
     * @return string Unprefixed configuration value.
     */
    public function getRealName(): string {
        return $this->info->getName();
    }

    public function getType(): string {
        return $this->info->getType();
    }

    public function isString(): bool {
        return $this->info->isString();
    }

    public function isInteger(): bool {
        return $this->info->isInteger();
    }

    public function isFloat(): bool {
        return $this->info->isFloat();
    }

    public function isBoolean(): bool {
        return $this->info->isBoolean();
    }

    public function isArray(): bool {
        return $this->info->isArray();
    }

    public function getValue(): mixed {
        return $this->info->getValue();
    }

    public function getString(): string {
        return $this->info->getString();
    }

    public function getInteger(): int {
        return $this->info->getInteger();
    }

    public function getFloat(): float {
        return $this->info->getFloat();
    }

    public function getBoolean(): bool {
        return $this->info->getBoolean();
    }

    public function getArray(): array {
        return $this->info->getArray();
    }

    public function __toString(): string {
        return (string)$this->info;
    }
}
