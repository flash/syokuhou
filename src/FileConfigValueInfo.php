<?php
// FileConfigValueInfo.php
// Created: 2023-10-20
// Updated: 2024-06-03

namespace Syokuhou;

class FileConfigValueInfo implements IConfigValueInfo {
    private string $name;
    private string $value;

    /** @internal */
    public function __construct(string $name, string $value = '') {
        $this->name = $name;
        $this->value = trim($value);
    }

    public function getName(): string {
        return $this->name;
    }

    public function getType(): string {
        // SharpChat config format is just all strings and casts on demand
        return 'string';
    }

    public function isString(): bool {
        return true;
    }

    public function isInteger(): bool {
        return true;
    }

    public function isFloat(): bool {
        return true;
    }

    public function isBoolean(): bool {
        return true;
    }

    public function isArray(): bool {
        return true;
    }

    public function getValue(): mixed {
        return $this->value;
    }

    public function getString(): string {
        return $this->value;
    }

    public function getInteger(): int {
        return (int)$this->value;
    }

    public function getFloat(): float {
        return (float)$this->value;
    }

    public function getBoolean(): bool {
        return $this->value !== '0'
            && strcasecmp($this->value, 'false') !== 0;
    }

    public function getArray(): array {
        return explode(' ', $this->value);
    }

    public function __toString(): string {
        return $this->value;
    }
}
